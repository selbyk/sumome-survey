# Sumome Survey

```json
{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Client Survey",
    "description": "Sumome interview coding test with the following requirements: 1. allow an admin to enter survey questions with multiple choice answers; 2. present a random survey question to guest and allow them to answer; 3. Record answers and display the survey results in an admin interface; 4. Avoid showing a previously answered question to the same guest. This API specification is used to generate the backend side implementation, and number four is taken care of by local storage on the front end.",
    "contact": {
      "name": "Selby Kendrick",
      "url": "https://selby.io"
    },
    "license": {
      "name": "MIT",
      "url": "http://creativecommons.org/licenses/by/4.0/"
    }
  },
  "host": "localhost:8000",
  "basePath": "/",
  "schemes": ["http"],
  "consumes": ["application/json"],
  "produces": ["application/json"],
    "security": [
    {
      "githubAccessCode": [
        "user",
        "user:email",
        "user:follow",
        "public_repo",
        "repo",
        "repo_deployment",
        "repo:status",
        "delete_repo",
        "notifications",
        "gist",
        "read:repo_hook",
        "write:repo_hook",
        "admin:repo_hook",
        "read:org",
        "write:org",
        "admin:org",
        "read:public_key",
        "write:public_key",
        "admin:public_key"
      ]
    },
    {
      "petstoreImplicit": [
        "user",
        "user:email",
        "user:follow",
        "public_repo",
        "repo",
        "repo_deployment",
        "repo:status",
        "delete_repo",
        "notifications",
        "gist",
        "read:repo_hook",
        "write:repo_hook",
        "admin:repo_hook",
        "read:org",
        "write:org",
        "admin:org",
        "read:public_key",
        "write:public_key",
        "admin:public_key"
      ]
    },
    {
      "internalApiKey": []
    }
  ],
  "securityDefinitions": {
    "githubAccessCode": {
      "type": "oauth2",
      "scopes": {
        "user": "Grants read/write access to profile info only. Note that this scope includes user:email and user:follow.",
        "user:email": "Grants read access to a user’s email addresses.",
        "user:follow": "Grants access to follow or unfollow other users.",
        "public_repo": "Grants read/write access to code, commit statuses, and deployment statuses for public repositories and organizations.",
        "repo": "Grants read/write access to code, commit statuses, and deployment statuses for public and private repositories and organizations.",
        "repo_deployment": "Grants access to deployment statuses for public and private repositories. This scope is only necessary to grant other users or services access to deployment statuses, without granting access to the code.",
        "repo:status": "Grants read/write access to public and private repository commit statuses. This scope is only necessary to grant other users or services access to private repository commit statuses without granting access to the code.",
        "delete_repo": "Grants access to delete adminable repositories.",
        "notifications": "Grants read access to a user’s notifications. repo also provides this access.",
        "gist": "Grants write access to gists.",
        "read:repo_hook": "Grants read and ping access to hooks in public or private repositories.",
        "write:repo_hook": "Grants read, write, and ping access to hooks in public or private repositories.",
        "admin:repo_hook": "Grants read, write, ping, and delete access to hooks in public or private repositories.",
        "read:org": "Read-only access to organization, teams, and membership.",
        "write:org": "Publicize and unpublicize organization membership.",
        "admin:org": "Fully manage organization, teams, and memberships.",
        "read:public_key": "List and view details for public keys.",
        "write:public_key": "Create, list, and view details for public keys.",
        "admin:public_key": "Fully manage public keys."
      },
      "flow": "accessCode",
      "authorizationUrl": "https://github.com/login/oauth/authorize",
      "tokenUrl": "https://github.com/login/oauth/access_token"
    },
    "petstoreImplicit": {
      "type": "oauth2",
      "scopes": {
        "user": "Grants read/write access to profile info only. Note that this scope includes user:email and user:follow.",
        "user:email": "Grants read access to a user’s email addresses.",
        "user:follow": "Grants access to follow or unfollow other users.",
        "public_repo": "Grants read/write access to code, commit statuses, and deployment statuses for public repositories and organizations.",
        "repo": "Grants read/write access to code, commit statuses, and deployment statuses for public and private repositories and organizations.",
        "repo_deployment": "Grants access to deployment statuses for public and private repositories. This scope is only necessary to grant other users or services access to deployment statuses, without granting access to the code.",
        "repo:status": "Grants read/write access to public and private repository commit statuses. This scope is only necessary to grant other users or services access to private repository commit statuses without granting access to the code.",
        "delete_repo": "Grants access to delete adminable repositories.",
        "notifications": "Grants read access to a user’s notifications. repo also provides this access.",
        "gist": "Grants write access to gists.",
        "read:repo_hook": "Grants read and ping access to hooks in public or private repositories.",
        "write:repo_hook": "Grants read, write, and ping access to hooks in public or private repositories.",
        "admin:repo_hook": "Grants read, write, ping, and delete access to hooks in public or private repositories.",
        "read:org": "Read-only access to organization, teams, and membership.",
        "write:org": "Publicize and unpublicize organization membership.",
        "admin:org": "Fully manage organization, teams, and memberships.",
        "read:public_key": "List and view details for public keys.",
        "write:public_key": "Create, list, and view details for public keys.",
        "admin:public_key": "Fully manage public keys."
      },
      "flow": "implicit",
      "authorizationUrl": "http://petstore.swagger.io/oauth/dialog"
    },
    "internalApiKey": {
      "type": "apiKey",
      "in": "header",
      "name": "api_key"
    }
  },
  "paths": {
    "/survey": {
      "get": {
        "tags": ["Survey Operations"],
        "summary": "Returns survey list",
        "responses": {
          "200": {
            "description": "pet response",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Survey"
              }
            },
            "headers": {
              "x-expires": {
                "type": "string"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/survey/{id}": {
      "get": {
        "tags": ["Survey Operations"],
        "summary": "Returns survey",
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "ID of survey responses to fetch",
          "required": true,
          "type": "integer",
          "format": "int64"
        }],
        "responses": {
          "200": {
            "description": "survvery response",
            "schema": {
              "$ref": "#/definitions/Survey"
            },
            "headers": {
              "x-expires": {
                "type": "string"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/survey/{id}/response": {
      "get": {
        "tags": ["Survey Operations"],
        "summary": "Returns next avaliable survey",
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "ID of survey responses to fetch",
          "required": true,
          "type": "integer",
          "format": "int64"
        }],
        "responses": {
          "200": {
            "description": "pet response",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Response"
              }
            },
            "headers": {
              "x-expires": {
                "type": "string"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/survey/{id}/option": {
      "get": {
        "tags": ["Survey Operations"],
        "summary": "Returns survey options",
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "ID of survey options to fetch",
          "required": true,
          "type": "integer",
          "format": "int64"
        }],
        "responses": {
          "200": {
            "description": "pet response",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Option"
              }
            },
            "headers": {
              "x-expires": {
                "type": "string"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/user": {
      "get": {
        "tags": ["User Operations"],
        "description": "Returns all pets from the system that the user has access to",
        "operationId": "get",
        "parameters": [{
          "name": "tags",
          "in": "query",
          "description": "tags to filter by",
          "required": false,
          "type": "array",
          "items": {
            "type": "string"
          },
          "collectionFormat": "csv"
        }, {
          "name": "limit",
          "in": "query",
          "description": "maximum number of results to return",
          "required": false,
          "type": "integer",
          "format": "int32"
        }],
        "responses": {
          "200": {
            "description": "User response",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/User"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "post": {
        "tags": ["User Operations"],
        "description": "Creates a new pet in the store.  Duplicates are allowed",
        "operationId": "post",
        "parameters": [{
          "name": "body",
          "description": "User to add to the store",
          "required": true,
          "schema": {
            "$ref": "#/definitions/User"
          }
        }],
        "responses": {
          "200": {
            "description": "user response",
            "schema": {
              "$ref": "#/definitions/User"
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/user/{id}": {
      "get": {
        "tags": ["User Operations"],
        "description": "Returns a user based on a single ID, if the user does not have access to the pet",
        "operationId": "findById",
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "ID of user to fetch",
          "required": true,
          "type": "integer",
          "format": "int64"
        }],
        "responses": {
          "200": {
            "description": "User response",
            "schema": {
              "$ref": "#/definitions/User"
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "delete": {
        "tags": ["User Operations"],
        "description": "deletes a single pet based on the ID supplied",
        "operationId": "deleteService",
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "ID of pet to delete",
          "required": true,
          "type": "integer",
          "format": "int64"
        }],
        "responses": {
          "204": {
            "description": "pet deleted",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/auth": {
      "post": {
        "tags": ["Authentication Methods"],
        "operationId": "user.auth",
        "summary": "authorize user",
        "responses": {
          "200": {
            "description": "user response",
            "schema": {
              "$ref": "#/definitions/Auth"
            },
            "headers": {
              "x-expires": {
                "type": "string"
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Auth": {
      "description": "Identity to perma track user from cookie setting",
      "type": "object",
      "properties": {
        "method": {
          "description": "Authentication method",
          "type": "string",
          "x-sequelize": {
            "unique": true,
            "primaryKey": true
          }
        },
        "data": {
          "description": "Data required by chosen auth method",
          "type": "object"
        }
      }
    },
    "Identity": {
      "description": "Identity to perma track user from cookie setting",
      "type": "object",
      "properties": {
        "identifier": {
          "description": "Unique hex value hash of keys",
          "type": "string",
          "x-sequelize": {
            "unique": true,
            "primaryKey": true
          }
        },
        "publicKey": {
          "type": "string"
        },
        "privateKey": {
          "type": "string"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Survey": {
      "description": "Survey question",
      "type": "object",
      "required": ["content"],
      "x-admin-only": ["responses"],
      "properties": {
        "question": {
          "type": "string"
        },
        "options": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Option"
          }
        },
        "responses": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Response"
          }
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Option": {
      "description": "question option",
      "type": "object",
      "required": ["answer"],
      "properties": {
        "survey": {
          "$ref": "#/definitions/Survey"
        },
        "answer": {
          "type": "string"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Response": {
      "description": "client resposne to question",
      "type": "object",
      "required": ["question",
        "identityHash",
        "option"
      ],
      "properties": {
        "question": {
          "$ref": "#/definitions/Survey"
        },
        "identityHash": {
          "$ref": "#/definitions/Identity"
        },
        "option": {
          "$ref": "#/definitions/Option"
        },
        "misc": {
          "type": "string"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "User": {
      "description": "User model",
      "type": "object",
      "required": ["email",
        "password"
      ],
      "properties": {
        "identityHash": {
          "$ref": "#/definitions/Identity"
        },
        "email": {
          "type": "string",
          "format": "email"
        },
        "password": {
          "type": "string",
          "format": "password"
        },
        "role": {
          "type": "string"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time"
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Info": {
      "description": "Info message",
      "type": "object",
      "required": ["code",
        "message"
      ],
      "properties": {
        "id": {
          "description": "Unique id",
          "type": "integer",
          "x-sequelize": {
            "unique": true,
            "primaryKey": true
          }
        },
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "time": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Error": {
      "description": "Error message",
      "type": "object",
      "required": ["code",
        "message"
      ],
      "properties": {
        "id": {
          "description": "Unique id",
          "type": "integer",
          "x-sequelize": {
            "unique": true,
            "primaryKey": true
          }
        },
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "time": {
          "type": "string",
          "format": "date-time"
        }
      }
    }
  }
}
```
