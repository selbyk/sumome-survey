'use strict';
var util = require('util');
var fs = require('fs');
var crypto = require('crypto');
var bcrypt = require('bcrypt');
var http = require('http');
var path = require('path');
var _ = require('lodash');
var feathers = require('feathers');
var winston = require('winston');
var feathersLogger = require('feathers-logger');
var Sequelize = require('sequelize');

var swaggerize = require('swaggerize-express');
var swaggerSequelize = require('./modules/swagger-sequelize');
var sequelizeService = require('feathers-sequelize');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var appModels = [];

var swaggerRoutes = feathers.Router();
var log = new(winston.Logger)({
  transports: [
    new(winston.transports.Console)(),
    //new (winston.transports.File)({ filename: 'somefile.log' })
  ]
});

var sequelize = new Sequelize('sequelize', '', '', {
  dialect: 'sqlite',
  storage: path.join(__dirname, 'db.sqlite'),
  define: {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  logging: log.info
});

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    sequelize.model('User')
      .findOne({
        where: {
          username: username
        }
      })
      .then(function(user) {
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        } else {
          bcrypt.compare(password, user.password, (valid)=>{
              return valid ? done(null, user) : done(null, false, { message: 'Incorrect password.' });
          });
        }
      }).catch((e) => {
        app.error(e);
        done(null, false, e);
      });
  }
));

var app = global.app = feathers()
  .configure(feathersLogger(log)) // Enable Socket.io
  .configure(feathers.socketio()) // Enable REST services
  .configure(feathers.rest());
  app.use(passport.initialize());
    app.use(passport.session());


app.use(cookieParser())
  .use(function(req, res, next) {
    res.sendStatus = function(status) {
      global.app.error('Really? lol');
      res.statusCode = status;
      res.statusMessage = http.STATUS_CODES[status];
      return res.send({
        error: {
          code: status,
          message: res.statusMessage
        }
      });
    };
    // /log.info(util.inspect(req,{depth: 5}));
    // var crypto = require('crypto');
    if(req.cookies.individualIdentifier){
      console.log("Returning client");
      console.log(req.cookies.individualIdentifier);
      next();
    } else {
      var primeLength = 128;
      var diffHell = crypto.createDiffieHellman(primeLength);
      diffHell.generateKeys('base64');
      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(diffHell.getPrivateKey('hex') + diffHell.getPublicKey('hex'), salt, function(err, hash) {
          // Store hash in your password DB.
          sequelize.model('Identity')
            .create({
              identifier: hash,
              publicKey: diffHell.getPublicKey('hex'),
              privateKey: diffHell.getPrivateKey('hex')
            })
            .then(function() {
              sequelize.model('Identity')
                .findOne({
                  where: {
                    identifier: hash
                  }
                })
                .then(function(identity) {
                  console.log(identity.identifier);
                  console.log(identity.publicKey);
                  console.log(identity.privateKey);
                  res.cookie('individualIdentifier', identity.identifier);
                  next();
                }).catch((e) => {
                  app.error(e);
                });
            }).catch((e) => {
              app.error(e);
            });
        });
      });
    }




  });


// a middleware function with no mount path. This code is executed for every request to the router
swaggerRoutes.use(function(req, res, next) {
  // /log.info(util.inspect(req,{depth: 5}));
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, PATCH, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});





var specPath = path.resolve('./config/surveyapi.json');
app.info(specPath);
var swaggerSpec = JSON.parse(fs.readFileSync(specPath), 'utf-8');
//app.info(swaggerSpec);
app.use(bodyParser.json()) //Turn on URL-encoded parser for REST services
  .use(bodyParser.urlencoded({
    extended: true
  }))
  .use(swaggerize({
    api: specPath,
    handlers: path.resolve('./handlers')
  }));
//var config = {
//appRoot: __dirname // required config
//};

//app.info(JSON.stringify(swaggerSpec, null, 2));
_.forOwn(swaggerSpec.definitions,
  function(modelSpec, modelName) {
    let modelRelations = {
      belongsTo: [],
      hasMany: [],
      oneToMany: [],
      manyToOne: []
    };
    app.info('modelSpec', JSON.stringify(modelSpec, null, 2));
    let modelProperties = _.omit(modelSpec, 'properties');
    modelProperties.properties = _.chain(modelSpec.properties)
      .pick(function(p) {
        app.info('p', p);
        if (p['$ref']) {
          modelRelations.belongsTo.push(p['$ref'].slice(14));
          return false;
        } else if (p.items && p.items['$ref']) {
          modelRelations.hasMany.push(p.items['$ref'].slice(14));
          return false;
        }
        return true;
      })
      //  .omit((t)=>{return t==={};})
      .value();
    app.info('modelProperties', util.inspect(modelProperties.properties, {
      depth: 3
    }));
    let modelOptions = swaggerSequelize.generate(modelProperties);
    if (modelOptions['id']) {
      modelOptions.id.primaryKey = true;
    }
    //modelOptions.swaggerRelations = modelRelations;
    app.info(modelName, ' options: ', JSON.stringify(modelOptions, null, 2));
    app.info(modelName, JSON.stringify(modelRelations, null, 2));
    let model = sequelize.define(modelName, modelOptions, {
      classMethods: {
        swagger: modelRelations
      }
    });

    app.info('Name', util.inspect(model.atrributes, {
      depth: 5
    }));

    app.info('SLKDM', util.inspect(sequelize.models[modelName], {
      depth: 3
    }));

    appModels.push({
      modelName: modelName,
      route: modelName.toLowerCase()
    });
    app.use('/' + modelName.toLowerCase(), sequelizeService({
      Model: sequelize.models[modelName],
      paginate: {
        default: 2,
        max: 4
      }
    }));
  });
//app.info('SDLSLKDMSLKDM', util.inspect(appModels, {
////  depth: 5
//}));
_.forEach(appModels,
  function(modelClass) {
    modelClass = sequelize.model(modelClass.modelName);
    _.forEach(modelClass.swagger.belongsTo,
      function(otherModelClass) {
        modelClass.belongsTo(sequelize.model(otherModelClass));
      });
    _.forEach(modelClass.swagger.hasMany,
      function(otherModelClass) {
        modelClass.hasMany(sequelize.model(otherModelClass));
      });
    /*    _.forIn(modelClass, function(value, key) {
            app.info(key);
          });*/
    app.info('Model', util.inspect(modelClass.name, null, 2));
    //app.info('Attrs', util.inspect(modelClass.attributes, null, 2));
    // /app.info('Rel', util.inspect(modelClass.swagger, null, 2));
    app.info(' Rel', util.inspect(modelClass.associations, null, 2));

  });

  app.use('/user/auth', function(req, res) {
    //res.sendStatus(501);

    //  if (!req.user || req.user.role !== 'Admin') {
    //  global.app.error('GET OUTTTA HERE');
    //  res.sendStatus(403);
    //} else {
    if(req.body.user){
    req.login(req.body.user, function(err) {
      if (err) {
        return res.sendStatus(403); //next(err);
      }
      return res.send(req.user);
    });}
      return res.sendStatus(403);
});

// Serve public folder for everybody
app
  .use('/', feathers.static(__dirname + '/docs/swagger-ui'))
  .use('/p', feathers.static(__dirname + '/public'))
  .use('/edit', feathers.static(__dirname + '/public/editor'))
  .listen(5644, () => {
    sequelize.sync({
      force: true
    });
    app.info('info', 'Your params are:', {
      foo: 'bar'
    });
    app.info('info', 'fuck');
  });


module.exports = app; // for testing
